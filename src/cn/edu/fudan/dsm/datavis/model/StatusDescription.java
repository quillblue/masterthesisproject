package cn.edu.fudan.dsm.datavis.model;

/**
 * Created by Sharon on 2015-07-01.
 */
public enum StatusDescription {
    MIN("min"),MAX("max"),SUM("sum"),AVG("average"),LEFT("left"),RIGHT("right");
    private final String description;

    private StatusDescription(String description){
        this.description=description;
    }

    public String getDescription() {
        return description;
    }
}

