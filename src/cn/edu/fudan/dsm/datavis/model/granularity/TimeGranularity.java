package cn.edu.fudan.dsm.datavis.model.granularity;

/**
 * Created by Sharon on 2015-07-01.
 */
public enum  TimeGranularity implements Granularity {
    ORIGINAL("original",1),SECOND("second",1),MINUTE("minute",60),HOUR("hour",3600),DAY("day",86400),MONTH("month",31*86400),YEAR("year",365*86400);

    private final String description;
    private final int multiplier;

    private TimeGranularity(String description, int multiplier){
        this.description=description;
        this.multiplier=multiplier;
    }

    public String getDescription() {
        return description;
    }

    public int getMultiplier() {
        return multiplier;
    }
}
