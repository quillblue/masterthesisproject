package cn.edu.fudan.dsm.datavis.model;

/**
 * Created by Sharon on 2015-07-01.
 */
public class DataItem<K extends Comparable,V>{
    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }

    private K key;
    private V value;

    @Override
    public String toString() {
        return String.format("%s %s",key.toString(),value.toString());
    }

}
