package cn.edu.fudan.dsm.datavis.model;

import java.util.List;

/**
 * Created by Sharon on 2015-07-26.
 */
public class QueryRange {
    private Object startKey;
    private Object endKey;
    private List<Object> addtionalControl;

    public QueryRange(Object startKey,Object endKey){
        this.startKey=startKey;
        this.endKey=endKey;
    }

    public QueryRange() {

    }

    public Object getStartKey() {
        return startKey;
    }

    public void setStartKey(Object startKey) {
        this.startKey = startKey;
    }

    public Object getEndKey() {
        return endKey;
    }

    public void setEndKey(Object endKey) {
        this.endKey = endKey;
    }

    public List<Object> getAddtionalControl() {
        return addtionalControl;
    }

    public void setAddtionalControl(List<Object> addtionalControl) {
        this.addtionalControl = addtionalControl;
    }
}
