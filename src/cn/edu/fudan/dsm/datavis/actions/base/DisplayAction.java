package cn.edu.fudan.dsm.datavis.actions.base;

import cn.edu.fudan.dsm.basic.action.BaseAction;
import net.sf.json.JSONObject;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by Sharon on 2015/10/1.
 */
public abstract class DisplayAction implements ServletRequestAware {
    protected JSONObject ret=new JSONObject();
    protected int r;
    protected String msg="";
    protected JSONObject data=new JSONObject();

    protected void ajaxResponse() {
        ret.put("r", r);
        if (r != 1) {
            data = null;
            ret.put("msg", msg);
        } else {
            ret.put("data", data);
        }
        HttpServletResponse response = ServletActionContext.getResponse();
        response.setCharacterEncoding("utf-8");
        PrintWriter pw = null;
        try {
            pw = response.getWriter();
            pw.write(ret.toString());
            pw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (pw != null) {
                pw.close();
            }
        }
    }

    /**
     *
     * @Title: getParam
     * @Description: 获取请求参数
     * @param key
     * @return
     */
    protected String getParam(String key) {
        return ServletActionContext.getRequest().getParameter(key);
    }

    @Override
    public void setServletRequest(HttpServletRequest arg0) {
        if (getParam("rand") != null) {
            // ajax请求
            ret = new JSONObject();
            r = 1;
            data = new JSONObject();
        }
    }

    protected int MAX_POINT_NUMER=5000;

    protected List<String> logicLogList;

    /**
     *
     * @Title: display
     * @Description: 为图表显示数据
     */
    public abstract void display();
}
