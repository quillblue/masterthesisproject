package cn.edu.fudan.dsm.datavis.actions;

import cn.edu.fudan.dsm.basic.util.DateUtil;
import cn.edu.fudan.dsm.datavis.actions.base.DisplayAction;
import cn.edu.fudan.dsm.datavis.algo.display.DisplayStrategyFactory;
import cn.edu.fudan.dsm.datavis.algo.grain.QueryStrategyFactory;
import cn.edu.fudan.dsm.datavis.algo.prefetch.IPrefetchStrategy;
import cn.edu.fudan.dsm.datavis.algo.prefetch.impl.DefaultPrefetchStrategyImpl;
import cn.edu.fudan.dsm.datavis.dal.PreFetchCache;
import cn.edu.fudan.dsm.datavis.model.DataItem;
import cn.edu.fudan.dsm.datavis.model.QueryRange;
import cn.edu.fudan.dsm.datavis.service.QueryService;
import cn.edu.fudan.dsm.datavis.service.impl.QueryServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

/**
 * Created by Sharon on 2015-08-10.
 */
public class TimeSeriesLineDisplayAction extends DisplayAction {

    private QueryService queryService=new QueryServiceImpl();

    private PreFetchCache preFetchCache=PreFetchCache.getInstance();

    @Override
    public void display() {

        long startTime=Long.valueOf(getParam("startTime"));
        long endTime=Long.valueOf(getParam("endTime"));
        QueryRange queryRange=new QueryRange();
        queryRange.setStartKey(startTime);
        queryRange.setEndKey(endTime);
        List<DataItem> queryResultList=queryService.query(queryRange, "TIME");
        List<Object[]>resultSet=new ArrayList<Object[]>();
        for(DataItem dataItem:queryResultList){
            Object[] returnItem=new Object[2];
            returnItem[0]=dataItem.getKey();
            returnItem[1]=dataItem.getValue();
            resultSet.add(returnItem);
        }
        data.put("list", resultSet);
        IPrefetchStrategy prefetchStrategy=new DefaultPrefetchStrategyImpl();
        String key="asdf";
        data.put("fetchKey",key);
        super.ajaxResponse();
        List<QueryRange> prefetchRanges=prefetchStrategy.getPrefetchRange(queryResultList);
        List<List<DataItem>> preFetchItem=new ArrayList<List<DataItem>>();
        for(QueryRange qr:prefetchRanges){
            preFetchItem.add(queryService.query(queryRange,"TIME"));
        }
        preFetchCache.set(key,preFetchItem);

    }

    public void fetchPreCache(){
        String key=getParam("key");
        List<List<DataItem>> resultList=preFetchCache.get(key);
        if(resultList==null){resultList=new ArrayList<List<DataItem>>();}
        data.put("list", resultList);
        super.ajaxResponse();
    }

}
