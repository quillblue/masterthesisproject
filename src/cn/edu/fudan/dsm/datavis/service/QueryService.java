package cn.edu.fudan.dsm.datavis.service;

import cn.edu.fudan.dsm.datavis.algo.display.IDisplayStrategy;
import cn.edu.fudan.dsm.datavis.algo.prefetch.IPrefetchStrategy;
import cn.edu.fudan.dsm.datavis.model.DataItem;
import cn.edu.fudan.dsm.datavis.model.QueryRange;

import java.util.List;

/**
 * Created by Sharon on 2015/10/6.
 */
public interface QueryService {
    public List<DataItem> query(QueryRange queryRange, String queryStrategyName);
}
