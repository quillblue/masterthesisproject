package cn.edu.fudan.dsm.datavis.service.impl;

import cn.edu.fudan.dsm.datavis.algo.display.IDisplayStrategy;
import cn.edu.fudan.dsm.datavis.algo.grain.IQueryStrategy;
import cn.edu.fudan.dsm.datavis.algo.grain.QueryStrategyFactory;
import cn.edu.fudan.dsm.datavis.algo.prefetch.IPrefetchStrategy;
import cn.edu.fudan.dsm.datavis.dal.MysqlDAO;
import cn.edu.fudan.dsm.datavis.dal.MysqlDAOImpl;
import cn.edu.fudan.dsm.datavis.model.DataItem;
import cn.edu.fudan.dsm.datavis.model.QueryRange;
import cn.edu.fudan.dsm.datavis.model.granularity.Granularity;
import cn.edu.fudan.dsm.datavis.service.QueryService;

import java.util.Date;
import java.util.List;

/**
 * Created by Sharon on 2015-07-07.
 */
public class QueryServiceImpl implements QueryService {

    @Override
    public List<DataItem> query(QueryRange queryRange, String queryStrategyName) {
        MysqlDAO dao=new MysqlDAOImpl();
        IQueryStrategy queryStrategy=new QueryStrategyFactory().getQueryStargey(QueryStrategyFactory.QueryStartegy.TIME);
        Granularity granularity=queryStrategy.getStrategy(queryRange, 1);
        Date starttime=new Date((long)queryRange.getStartKey());
        Date endtime=new Date((long)queryRange.getEndKey());
        return dao.queryData(starttime,endtime,granularity);
    }
}
