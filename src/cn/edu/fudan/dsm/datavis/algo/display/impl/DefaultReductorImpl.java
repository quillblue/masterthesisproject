package cn.edu.fudan.dsm.datavis.algo.display.impl;

import cn.edu.fudan.dsm.datavis.algo.display.IDisplayStrategy;
import cn.edu.fudan.dsm.datavis.model.DataItem;

import java.util.List;

/**
 * Created by Sharon on 2015-07-16.
 */
public class DefaultReductorImpl implements IDisplayStrategy{

    @Override
    public List<DataItem> postProgress(List<DataItem> queryResult) {
        return queryResult;
    }
}
