package cn.edu.fudan.dsm.datavis.algo.display;

import cn.edu.fudan.dsm.datavis.algo.display.IDisplayStrategy;
import cn.edu.fudan.dsm.datavis.algo.display.impl.DefaultReductorImpl;
import cn.edu.fudan.dsm.datavis.algo.display.impl.M4ReductorImpl;

/**
 * Created by Sharon on 2015-07-07.
 */
public class DisplayStrategyFactory {
    public enum DisplayStrategy{DEFAULT,M4Reductor};

    public IDisplayStrategy getStrategy(DisplayStrategy strategy){
        switch(strategy){
            case DEFAULT:return new DefaultReductorImpl();
            case M4Reductor:return new M4ReductorImpl();
            default:return new DefaultReductorImpl();
        }
    }
}
