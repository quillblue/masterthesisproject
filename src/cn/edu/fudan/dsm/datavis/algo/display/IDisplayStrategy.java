package cn.edu.fudan.dsm.datavis.algo.display;

import cn.edu.fudan.dsm.datavis.model.DataItem;

import java.util.List;

/**
 * Created by Sharon on 2015-07-06.
 */
public interface IDisplayStrategy {
    public List<DataItem> postProgress(List<DataItem> queryResult);
}