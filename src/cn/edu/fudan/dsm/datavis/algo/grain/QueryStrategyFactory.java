package cn.edu.fudan.dsm.datavis.algo.grain;

import cn.edu.fudan.dsm.datavis.algo.grain.impl.DefaultQueryStrategy;
import cn.edu.fudan.dsm.datavis.algo.grain.impl.TimeQueryStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Sharon on 2015-07-06.
 */
@Component
public class QueryStrategyFactory {

    public enum QueryStartegy{DEFAULT,TIME}

    public IQueryStrategy getQueryStargey(QueryStartegy queryStartegy){
        switch (queryStartegy){
            case TIME:return new TimeQueryStrategy();
            case DEFAULT:return new DefaultQueryStrategy();
            default:return new DefaultQueryStrategy();
        }
    }

}
