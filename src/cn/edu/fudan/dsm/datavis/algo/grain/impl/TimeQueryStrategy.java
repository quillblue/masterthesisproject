package cn.edu.fudan.dsm.datavis.algo.grain.impl;

import cn.edu.fudan.dsm.datavis.algo.grain.IQueryStrategy;
import cn.edu.fudan.dsm.datavis.model.QueryRange;
import cn.edu.fudan.dsm.datavis.model.granularity.Granularity;
import cn.edu.fudan.dsm.datavis.model.granularity.TimeGranularity;

/**
 * Created by Sharon on 2015-07-06.
 */
public class TimeQueryStrategy implements IQueryStrategy {

    private static final int MAX_POINT=30000;

    @Override
    public Granularity getStrategy(QueryRange queryRange, int density) {
        long queryDuringTime=(long)queryRange.getEndKey()-(long)queryRange.getStartKey();
        long estimateResult=queryDuringTime/1000*density;
        if(estimateResult<MAX_POINT){return TimeGranularity.ORIGINAL;}
        else {
            for (TimeGranularity timeGranularity : TimeGranularity.values()) {
                if(estimateResult/timeGranularity.getMultiplier()<MAX_POINT){
                    return timeGranularity;
                }
            }
        }
        return TimeGranularity.YEAR;
    }
}
