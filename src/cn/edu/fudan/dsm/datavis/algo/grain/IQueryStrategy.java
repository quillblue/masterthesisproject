package cn.edu.fudan.dsm.datavis.algo.grain;

import cn.edu.fudan.dsm.datavis.model.QueryRange;
import cn.edu.fudan.dsm.datavis.model.granularity.Granularity;

/**
 * Created by Sharon on 2015-07-06.
 */
public interface IQueryStrategy {
    public Granularity getStrategy(QueryRange queryRange,int density);
}
