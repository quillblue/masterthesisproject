package cn.edu.fudan.dsm.datavis.algo.prefetch.impl;

import cn.edu.fudan.dsm.datavis.algo.prefetch.IPrefetchStrategy;
import cn.edu.fudan.dsm.datavis.model.DataItem;
import cn.edu.fudan.dsm.datavis.model.QueryRange;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sharon on 2015-07-16.
 */
public class DefaultPrefetchStrategyImpl implements IPrefetchStrategy {
    @Override
    public List<QueryRange> getPrefetchRange(List<DataItem> queryResult) {
        long startTime=(long)queryResult.get(0).getKey();
        long endTime=(long)queryResult.get(queryResult.size()-1).getKey();
        List<QueryRange> returnSet=new ArrayList<QueryRange>();
        returnSet.add(new QueryRange(startTime,startTime+1000));
        returnSet.add(new QueryRange(endTime-1000, endTime));
        return returnSet;
    }
}
