package cn.edu.fudan.dsm.datavis.algo.prefetch;

import cn.edu.fudan.dsm.datavis.model.DataItem;
import cn.edu.fudan.dsm.datavis.model.QueryRange;

import java.util.List;

/**
 * Created by Sharon on 2015-07-07.
 */
public interface IPrefetchStrategy {
    public List<QueryRange> getPrefetchRange(List<DataItem> queryResult);
}
