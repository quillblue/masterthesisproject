package cn.edu.fudan.dsm.datavis.dal;

import cn.edu.fudan.dsm.datavis.model.DataItem;
import cn.edu.fudan.dsm.datavis.model.granularity.Granularity;

import java.util.Date;
import java.util.List;

/**
 * Created by Sharon on 2015/10/1.
 */
public interface MysqlDAO {
    public List<DataItem> queryData(Date startTime, Date endTime, Granularity granularity);
}
