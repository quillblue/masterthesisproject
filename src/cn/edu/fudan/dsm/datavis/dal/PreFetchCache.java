package cn.edu.fudan.dsm.datavis.dal;

import cn.edu.fudan.dsm.datavis.model.DataItem;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by Sharon on 2015-07-01.
 */
public class PreFetchCache {
    private static PreFetchCache instance;
    private HashMap<String,List<List<DataItem>>> cache=new HashMap<String,List<List<DataItem>>>(100);
    private Queue<String> cacheMenu=new LinkedList<String>();
    private int maxLimit=100;

    public static PreFetchCache getInstance() {
        if(instance == null){
            instance =  new PreFetchCache();
        }
        return instance;
    }

    public List<List<DataItem>> get(String key){
        return cache.get(key);
    }

    public void set(String key,List<List<DataItem>> prefetchResult){
        if(cacheMenu.size()==maxLimit){
            String firstKey=cacheMenu.poll();
            cache.remove(firstKey);
        }
        cache.put(key,prefetchResult);
        cacheMenu.add(key);
    }

    public int getMaxLimit() {
        return maxLimit;
    }

    public void setMaxLimit(int maxLimit) {
        this.maxLimit = maxLimit;
    }
}
