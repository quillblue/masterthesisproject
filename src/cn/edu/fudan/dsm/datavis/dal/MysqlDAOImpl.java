package cn.edu.fudan.dsm.datavis.dal;

import cn.edu.fudan.dsm.datavis.model.DataItem;
import cn.edu.fudan.dsm.datavis.model.granularity.Granularity;
import cn.edu.fudan.dsm.datavis.model.granularity.TimeGranularity;

import java.sql.*;
import java.util.*;
import java.util.Date;

/**
 * Created by Sharon on 2015/10/1.
 */
public class MysqlDAOImpl implements MysqlDAO {


    public static final String CONNECTIONSTR="jdbc:mysql://127.0.0.1:3306/transport";
    public static final String ORIGINALTABLE="sptcc_usage";
    public static final String STATTABLE="sptcc_usage_stat";

    @Override
    public List<DataItem> queryData(Date startTime, Date endTime, Granularity granularity){
        List<DataItem> returnList=new LinkedList<DataItem>();
        String command="SELECT time,count FROM ";
        PreparedStatement pstmt=null;
        Connection conn=null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn=DriverManager.getConnection(CONNECTIONSTR,"root","quillmysql");
            if (granularity == TimeGranularity.ORIGINAL) {
                command = command + ORIGINALTABLE + " WHERE time>=? AND time<=?";
                pstmt=conn.prepareStatement(command);
                pstmt.setTimestamp(1, new Timestamp(startTime.getTime()));
                pstmt.setTimestamp(2, new Timestamp(endTime.getTime()));
            } else {
                command = command + STATTABLE + " WHERE time>=? AND time<=? AND granule=? ORDER BY time";
                pstmt=conn.prepareStatement(command);
                pstmt.setTimestamp(1, new Timestamp(startTime.getTime()));
                pstmt.setTimestamp(2, new Timestamp(endTime.getTime()));
                pstmt.setString(3, granularity.toString());
            }
            ResultSet rs=pstmt.executeQuery();
            while(rs.next()){
                DataItem dataItem=new DataItem();
                dataItem.setKey(rs.getTimestamp(1).getTime());
                dataItem.setValue(rs.getInt(2));
                returnList.add(dataItem);
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }finally {
            try{
                if(conn!=null){
                    conn.close();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            return returnList;
        }
    }

    public static void main(String[] args){
        MysqlDAO dao=new MysqlDAOImpl();
        List<DataItem> list=dao.queryData(new Date(115,3,1,4,4,4),new Date(115,3,5,4,4,4),TimeGranularity.MINUTE);
        for(DataItem item:list){
            System.out.println(item.toString());
        }
    }
}
