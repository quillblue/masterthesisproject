/**
* @Title: HbaseTable.java
* @Description: TODO
* @author: Calvinyang
* @date: Mar 29, 2015 10:10:21 AM
* Copyright: Copyright (c) 2013
* @version: 1.0
*/
package cn.edu.fudan.dsm.basic.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author: Calvinyang
 * @Description: TODO
 * @date: Mar 29, 2015 10:10:21 AM
 */
@Target({java.lang.annotation.ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface HbaseTable {
	public abstract String name();
}
