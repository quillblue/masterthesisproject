/**
* @Title: PropertyUtil.java
* @Description: TODO
* @author: Calvinyang
* @date: Mar 30, 2015 9:38:07 AM
* Copyright: Copyright (c) 2013
* @version: 1.0
*/
package cn.edu.fudan.dsm.basic.util;

import java.io.IOException;
import java.util.Properties;

/**
 * @author: Calvinyang
 * @Description: TODO
 * @date: Mar 30, 2015 9:38:07 AM
 */
public class PropertyUtil {
	private static Properties props = null; 
	
	static {
		props = new Properties();
		try {
			props.load(PropertyUtil.class.getResourceAsStream("/config.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	* @Title: getProperty
	* @Description: TODO
	* @param key
	* @return
	 */
	public static String getProperty(String key) {
		if (props == null) {
			return null;
		}
		if (key.startsWith("${")) {
			key = key.substring(2, key.length() - 1);
		}
		return props.getProperty(key, null);
	}
	
	public static int getInt(String key) {
		String v = getProperty(key);
		if (v != null) {
			return Integer.parseInt(v);
		}
		return 0;
	}
}
