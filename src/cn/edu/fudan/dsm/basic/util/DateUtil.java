/**
* @Title: DateUtil.java
* @Description: TODO
* @author: Calvinyang
* @date: Mar 29, 2015 12:58:05 PM
* Copyright: Copyright (c) 2013
* @version: 1.0
*/
package cn.edu.fudan.dsm.basic.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author: Calvinyang
 * @Description: TODO
 * @date: Mar 29, 2015 12:58:05 PM
 */
public class DateUtil {
	public static final String DATE_FORMAT = "yyyy-MM-dd";
	public static final String DATE_FORMAT_NEW = "yyyy/MM/dd";
	public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public static final String MERGE_FORMAT = "yyyyMMddHHmmss";

	/**
	 * 
	* @Title: getCurrentTime
	* @Description: 获取近似当前时间
	* @return
	 */
	public static String getCurrentTime() {
		return DateUtil.format(new Date(), DATE_TIME_FORMAT);
	}
	
	/**
	 * @throws PrjException
	 * 
	 * @Title: format
	 * @Description: 指定日期转换为指定格式
	 * @param: date
	 * @param: format
	 * @return: Date
	 * @throws
	 */
	public static Date format(String date, String format) {
		try {
			return new SimpleDateFormat(format).parse(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @throws PrjException
	 * 
	 * @Title: format
	 * @Description: 指定日期转换为默认格式
	 * @param: date
	 * @return: Date
	 * @throws
	 */
	public static Date format(String date) {
		return format(date, DATE_FORMAT);
	}

	/**
	 * 
	 * @Title: format
	 * @Description: 日期转换为指定格式
	 * @param: date
	 * @param: format
	 * @return: String
	 * @throws
	 */
	public static String format(Date date, String format) {
		return new SimpleDateFormat(format).format(date);
	}

	/**
	 * 
	 * @Title: format
	 * @Description: 日期转换为默认格式
	 * @param: date
	 * @return: String
	 * @throws
	 */
	public static String format(Date date) {
		return format(date, DATE_FORMAT);
	}
}
