/**
* @Title: SocketFactory.java
* @Description: TODO
* @author: Calvinyang
* @date: 2015年7月12日 下午9:01:17
* Copyright: Copyright (c) 2013
* @version: 1.0
*/
package cn.edu.fudan.dsm.basic.util;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: Calvinyang
 * @Description: socket连接池
 * @date: 2015年7月12日 下午9:01:17
 */
public class SocketFactory {
	private static SocketFactory instance;
	private List<Socket> sockets;
	private int used;
	private int minSize;
	private int maxSize;
	private String host;
	private int port;
	
	private SocketFactory() {
		minSize = PropertyUtil.getInt("socket.pool.min");
		maxSize = PropertyUtil.getInt("socket.pool.max");
		host = PropertyUtil.getProperty("socket.ip");
		port = PropertyUtil.getInt("socket.port");
	}
	
	/**
	 * 
	* @Title: init
	* @Description: init factory
	 */
	private void init() {
		sockets = new ArrayList<Socket>(minSize);
		for (int i = 0; i < minSize; i++) {
			try {
				Socket soc = new Socket(host, port);
				sockets.add(soc);
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * @return the instance
	 */
	public synchronized static SocketFactory getInstance() {
		if (instance == null) {
			instance = new SocketFactory();
			instance.init();
		}
		return instance;
	}
	
	// 归还一个连接
	public void releaseSocket(Socket sock) {
		if (!sock.isClosed()) {
			try {
				sock.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		sockets.add(sock);
	}
	
	/**
	 * 
	* @Title: getSocket
	* @Description: 获取一个连接
	* @return
	 */
	public Socket getSocket() {
		if (sockets.size() > 0) {
			Socket sock = sockets.get(0);
			sockets.remove(0);
			used++;
			if (sock.isClosed()) {
				try {
					sock = new Socket(host, port);
					sock.setSoTimeout(5000);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return sock;
		}
		// 如果不足则批量添加
		if (used < maxSize) {
			for(int i = used; i < maxSize ; i ++) {
				try {
					Socket soc = new Socket(host, port);
					sockets.add(soc);
					return getSocket();
				} catch (UnknownHostException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}
	
}
