/**
* @Title: BaseService.java
* @Description: TODO
* @author: Calvinyang
* @date: Mar 28, 2015 8:42:11 PM
* Copyright: Copyright (c) 2013
* @version: 1.0
*/
package cn.edu.fudan.dsm.basic.service;

import cn.edu.fudan.dsm.basic.bean.i.IBaseBean;
import cn.edu.fudan.dsm.basic.dao.i.IBaseDao;
import cn.edu.fudan.dsm.basic.model.CriteriaList;
import cn.edu.fudan.dsm.basic.service.i.IBaseService;

import java.util.List;

/**
 * @author: Calvinyang
 * @Description: TODO
 * @date: Mar 28, 2015 8:42:11 PM
 */
public abstract class BaseService<T extends IBaseBean> implements IBaseService<T> {
	protected IBaseDao<T> dao;

	@Override
	public int add(T obj) {
		return dao.add(obj);
	}

	@Override
	public T get(int id) {
		return dao.get(id);
	}
	
	@Override
	public T get(CriteriaList conditions) {
		return dao.get(conditions);
	}

	@Override
	public List<T> getList(CriteriaList conditions) {
		return dao.getList(conditions);
	}
	
	@Override
	public List<T> getList(CriteriaList conditions, int start, int limit) {
		return dao.getList(conditions, start, limit);
	}
}
