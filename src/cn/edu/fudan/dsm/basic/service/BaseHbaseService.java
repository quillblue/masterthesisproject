/**
* @Title: BaseHbaseService.java
* @Description: TODO
* @author: Calvinyang
* @date: Mar 28, 2015 11:06:39 PM
* Copyright: Copyright (c) 2013
* @version: 1.0
*/
package cn.edu.fudan.dsm.basic.service;

import cn.edu.fudan.dsm.basic.bean.i.IBaseHBaseBean;
import cn.edu.fudan.dsm.basic.dao.i.IBaseHbaseDao;
import cn.edu.fudan.dsm.basic.service.i.IBaseHbaseService;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author: Calvinyang
 * @Description: TODO
 * @date: Mar 28, 2015 11:06:39 PM
 */
public abstract class BaseHbaseService<T extends IBaseHBaseBean> implements IBaseHbaseService<T> {
	protected IBaseHbaseDao<T> dao;
	
	@Override
	public void createTables(String[] family) throws IOException {
		dao.createTables(family);
	}

	@Override
	public void addData(Put put) {
		dao.addData(put);
	}

	@Override
	public void addData(String key, String family, String qualifier, String value) {
		dao.addData(key, family, qualifier, value);
	}

	@Override
	public void addData(List<Put> list) {
		dao.addData(list);
	}

	@Override
	public T getData(String key) {
		Result result = dao.getData(key);
		T inst = null;
		try {
			inst = dao.getClazz().newInstance();
			inst.translate(result);
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return inst;
	}

	@Override
	public List<T> getData(String start, String end) {
		ResultScanner rs = dao.getData(start, end);
		return scannerToList(rs);
	}
	
	/**
	 * 
	* @Title: scannerToList
	* @Description: TODO
	* @param rs
	* @return
	 */
	private List<T> scannerToList(ResultScanner rs) {
		List<T> list = new LinkedList<T>();
		for(Result result : rs) {
			T inst = null;
			try {
				inst = dao.getClazz().newInstance();
				inst.translate(result);
				list.add(inst);
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		rs.close();
		return new ArrayList<T>(list);
	}

	@Override
	public List<T> getData(String start, String end, String family, String qualifier) {
		return scannerToList(dao.getData(start, end, family, qualifier));
	}

	@Override
	public List<T> getData(Scan scan) {
		return scannerToList(dao.getData(scan));
	}

}
