/**
* @Title: IBaseService.java
* @Description: TODO
* @author: Calvinyang
* @date: Mar 28, 2015 8:41:35 PM
* Copyright: Copyright (c) 2013
* @version: 1.0
*/
package cn.edu.fudan.dsm.basic.service.i;

import cn.edu.fudan.dsm.basic.bean.i.IBaseBean;
import cn.edu.fudan.dsm.basic.dao.i.IBaseDao;
import cn.edu.fudan.dsm.basic.model.CriteriaList;

import java.util.List;

/**
 * @author: Calvinyang
 * @Description: TODO
 * @date: Mar 28, 2015 8:41:35 PM
 */
public interface IBaseService<T extends IBaseBean> {
	/**
	 * 
	* @Title: setDao
	* @Description: TODO
	* @param dao
	 */
	void setDao(IBaseDao<T> dao);
	
	/**
	 * 
	* @Title: add
	* @Description: TODO
	* @param obj
	* @return
	 */
	int add(T obj);
	
	/**
	 * 
	* @Title: get
	* @Description: TODO
	* @return
	 */
	T get(int id);
	
	/**
	 * 
	* @Title: get
	* @Description: TODO
	* @param conditions
	* @return
	 */
	T get(CriteriaList conditions);
	
	/**
	 * 
	* @Title: getList
	* @Description: TODO
	* @return
	 */
	List<T> getList(CriteriaList conditions);
	
	/**
	 * 
	* @Title: getList
	* @Description: TODO
	* @param conditions
	* @param start
	* @param limit
	* @return
	 */
	List<T> getList(CriteriaList conditions, int start, int limit);
}
