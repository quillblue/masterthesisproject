/**
 * @Title: BaseAction.java
 * @Description: TODO
 * @author: Calvinyang
 * @date: Mar 28, 2015 8:20:14 PM
 * Copyright: Copyright (c) 2013
 * @version: 1.0
 */
package cn.edu.fudan.dsm.basic.action;

import cn.edu.fudan.dsm.basic.bean.i.IBaseBean;
import net.sf.json.JSONObject;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author: Calvinyang
 * @Description: TODO
 * @date: Mar 28, 2015 8:20:14 PM
 */
public class BaseAction<T extends IBaseBean> implements ServletRequestAware {
    protected JSONObject ret;
    protected int r=0;
    protected String msg;
    protected JSONObject data;

    protected void ajaxResponse() {
        ret.put("r", r);
        if (r != 1) {
            data = null;
            ret.put("msg", msg);
        } else {
            ret.put("data", data);
        }
        HttpServletResponse response = ServletActionContext.getResponse();
        response.setCharacterEncoding("utf-8");
        PrintWriter pw = null;
        try {
            pw = response.getWriter();
            pw.write(ret.toString());
            pw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (pw != null) {
                pw.close();
            }
        }
    }

    /**
     *
     * @Title: getParam
     * @Description: 获取请求参数
     * @param key
     * @return
     */
    protected String getParam(String key) {
        return ServletActionContext.getRequest().getParameter(key);
    }

    @Override
    public void setServletRequest(HttpServletRequest arg0) {
        if (getParam("rand") != null) {
            // ajax请求
            ret = new JSONObject();
            r = 1;
            data = new JSONObject();
        }
    }
}
