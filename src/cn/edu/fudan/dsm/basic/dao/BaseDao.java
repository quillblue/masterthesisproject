/**
* @Title: BaseDao.java
* @Description: TODO
* @author: Calvinyang
* @date: Mar 28, 2015 8:37:27 PM
* Copyright: Copyright (c) 2013
* @version: 1.0
*/
package cn.edu.fudan.dsm.basic.dao;

import cn.edu.fudan.dsm.basic.bean.i.IBaseBean;
import cn.edu.fudan.dsm.basic.dao.i.IBaseDao;
import cn.edu.fudan.dsm.basic.model.CriteriaList;
import org.hibernate.criterion.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: Calvinyang
 * @Description: TODO
 * @date: Mar 28, 2015 8:37:27 PM
 */
public abstract class BaseDao<T extends IBaseBean> implements IBaseDao<T> {
	@Autowired
	protected HibernateTemplate hibernateTemplate;
	
	@Override
	public int add(T obj) {
		try {
			hibernateTemplate.save(obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return obj.getId();
	}

	@Override
	public T get(int id) {
		return get(CriteriaList.create(Restrictions.eq("id", id)));
	}

	@SuppressWarnings("unchecked")
	public T get(CriteriaList conditions) {
		DetachedCriteria dc = conditions2Criteria(conditions);
		try {
			List<T> list = (List<T>) hibernateTemplate.findByCriteria(dc, 0, 1);
			if (list.size() > 0) {
				return list.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<T> getList(CriteriaList conditions) {
		DetachedCriteria dc = conditions2Criteria(conditions);
		try {
			return (List<T>) hibernateTemplate.findByCriteria(dc);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ArrayList<T>(0);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> getList(CriteriaList conditions, int start, int limit) {
		DetachedCriteria dc = conditions2Criteria(conditions);
		try {
			return (List<T>) hibernateTemplate.findByCriteria(dc, start, limit);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ArrayList<T>(0);
	}
	
	/**
	 * 
	* @Title: conditions2Criteria
	* @Description: 条件转criteria
	* @param conditions
	* @return
	 */
	private DetachedCriteria conditions2Criteria(CriteriaList conditions) {
		DetachedCriteria dc = DetachedCriteria.forClass(getClazz());
		if (null == conditions || conditions.size() == 0) {
			return dc;
		}
		for(Object obj : conditions) {
			if (obj instanceof Criterion) {
				dc.add((Criterion) obj);
			}
			else if (obj instanceof Order) {
				dc.addOrder((Order) obj);
			}
			else if (obj instanceof ProjectionList) {
				dc.setProjection((ProjectionList) obj);
			}
		}
		return dc;
	}

}
