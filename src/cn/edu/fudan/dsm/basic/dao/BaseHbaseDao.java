/**
 * @Title: BaseHbaseDao.java
 * @Description: TODO
 * @author: Calvinyang
 * @date: Mar 28, 2015 10:42:54 PM
 * Copyright: Copyright (c) 2013
 * @version: 1.0
 */
package cn.edu.fudan.dsm.basic.dao;

import cn.edu.fudan.dsm.basic.annotation.HbaseTable;
import cn.edu.fudan.dsm.basic.bean.i.IBaseHBaseBean;
import cn.edu.fudan.dsm.basic.dao.i.IBaseHbaseDao;
import cn.edu.fudan.dsm.basic.util.PropertyUtil;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.*;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.util.List;

/**
 * @author: Calvinyang
 * @Description: TODO
 * @date: Mar 28, 2015 10:42:54 PM
 */
public abstract class BaseHbaseDao<T extends IBaseHBaseBean> implements IBaseHbaseDao<T> {
	private static Configuration configuration;
	@SuppressWarnings("deprecation")
	protected static HTablePool htablePool;
	protected static HBaseAdmin hbaseAdmin;
	@Value("${hbase.conn.poolsize}")
	private int poolSize;
	@Value("${hbase.writebuffer.size}")
	private int writeBufferSize;
	@Value("${hbase.batch.size}")
	private int batchSize;
	@Value("${hbase.zookeeper.quorum}")
	private String zookeeper;
	@Value("${hbase.zookeeper.port}")
	private String port;

	/**
	 * 
	 * @Title: getConfiguration
	 * @Description: TODO
	 * @return
	 */
	synchronized protected Configuration getConfiguration() {
		if (configuration == null) {
			configuration = HBaseConfiguration.create();
			// 设置master地址
			configuration.set(HConstants.ZOOKEEPER_QUORUM, zookeeper);
			// 配置
//			configuration.set(HConstants.ZOOKEEPER_CLIENT_PORT, port);
//			configuration.set(HConstants.HBASE_CLIENT_SCANNER_TIMEOUT_PERIOD, "60000");
//			configuration.set(HConstants.HBASE_REGIONSERVER_LEASE_PERIOD_KEY, "60000");
//			configuration.set(HConstants.HBASE_RPC_TIMEOUT_KEY, "60000");
//			configuration.set(HConstants.HBASE_RPC_SHORTOPERATION_TIMEOUT_KEY, "60000");
		}
		return configuration;
	}

	/**
	 * @return the htablePool
	 */
	@SuppressWarnings("deprecation")
	synchronized protected HTablePool getHtablePool() {
		if (htablePool == null) {
			htablePool = new HTablePool(getConfiguration(), poolSize);
		}
		return htablePool;
	}

	/**
	 * @return the hbaseAdmin
	 */
	synchronized protected HBaseAdmin getHbaseAdmin() {
		if (hbaseAdmin == null) {
			try {
				hbaseAdmin = new HBaseAdmin(configuration);
			} catch (MasterNotRunningException e) {
				e.printStackTrace();
			} catch (ZooKeeperConnectionException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					hbaseAdmin.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return hbaseAdmin;
	}

	@Override
	public void createTables(String[] family) throws IOException {
		if (!hbaseAdmin.tableExists(getTableName())) {
			HTableDescriptor descriptor = new HTableDescriptor(getTableName());
			for (String f : family) {
				descriptor.addFamily(new HColumnDescriptor(f));
			}
			hbaseAdmin.createTable(descriptor);
		}
	}

	/**
	* @Title: getTableName
	* @Description: 获取要操作的表名
	* @return
	*/
	private String getTableName() {
		HbaseTable ht = getClazz().getAnnotation(HbaseTable.class);
		String placeholder = ht != null ? ht.name() : null;
		return PropertyUtil.getProperty(placeholder);
	}

	@Override
	public void addData(Put put) {
		HTableInterface table = getHtable();
		try {
			table.put(put);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				table.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 
	 * @Title: getHtable
	 * @Description: 获取htable对象
	 * @return
	 */
	@SuppressWarnings("deprecation")
	synchronized HTableInterface getHtable() {
		HTableInterface table = getHtablePool().getTable(getTableName());
		try {
			table.setWriteBufferSize(writeBufferSize * 1024 * 1024);
			table.setAutoFlush(false);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return table;
	}

	@Override
	public void addData(String key, String family, String qualifier, String value) {
		Put put = new Put(Bytes.toBytes(key));
		put.add(Bytes.toBytes(family), Bytes.toBytes(qualifier), Bytes.toBytes(value));
		addData(put);
	}

	@Override
	public void addData(List<Put> list) {
		HTableInterface table = getHtable();
		try {
			table.put(list);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				table.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public Result getData(String key) {
		HTableInterface table = getHtable();
		Get get = new Get(Bytes.toBytes(key));
		try {
			return table.get(get);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public ResultScanner getData(Scan scan) {
		scan.setBatch(batchSize);
		scan.setCacheBlocks(false);
		HTableInterface table = getHtable();
		try {
			return table.getScanner(scan);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				table.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	@Override
	public ResultScanner getData(String start, String end) {
		Scan scan = new Scan(Bytes.toBytes(start), Bytes.toBytes(end));
		return getData(scan);
	}

	@Override
	public ResultScanner getData(String start, String end, String family, String qualifier) {
		Scan scan = new Scan(Bytes.toBytes(start), Bytes.toBytes(end));
		if (family != null) {
			if (qualifier != null) {
				String[] tmp = qualifier.split(",");
				for(String q : tmp) {
					if (q.length() > 0) {
						scan.addColumn(Bytes.toBytes(family), Bytes.toBytes(q.trim()));
					}
				}
			}
			else {
				scan.addFamily(Bytes.toBytes(family));
			}
		}
		return getData(scan);
	}

}
