/**
* @Title: IBaseHbaseDao.java
* @Description: TODO
* @author: Calvinyang
* @date: Mar 28, 2015 10:40:02 PM
* Copyright: Copyright (c) 2013
* @version: 1.0
*/
package cn.edu.fudan.dsm.basic.dao.i;

import cn.edu.fudan.dsm.basic.bean.i.IBaseHBaseBean;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;

import java.io.IOException;
import java.util.List;

/**
 * @author: Calvinyang
 * @Description: TODO
 * @date: Mar 28, 2015 10:40:02 PM
 */
public interface IBaseHbaseDao<T extends IBaseHBaseBean> {
	/**
	 * 
	* @Title: getTableName
	* @Description:操作的表名
	* @return
	 */
	Class<T> getClazz();
	
	/**
	 * 
	 * @throws PrjException 
	 * @throws IOException 
	 * @Title: createTables
	 * @Description: 创建一个表
	 * 
	 */
	void createTables(String[] family) throws IOException;
	
	/**
	 * 
	* @Title: addData
	* @Description: 添加一行数据
	* @param put
	 * @throws IOException 
	 */
	void addData(Put put);
	
	/**
	 * 
	* @Title: addData
	* @Description: 添加一行数据
	* @param key
	* @param family
	* @param qualifier
	* @param value
	 */
	void addData(String key, String family, String qualifier, String value);
	
	/**
	 * 
	* @Title: addData
	* @Description: 添加多条数据
	* @param list
	 */
	void addData(List<Put> list);
	
	/**
	 * 
	* @Title: getData
	* @Description: 查询一行数据
	* @param key
	* @return
	 */
	Result getData(String key);
	
	/**
	 * 
	* @Title: getData
	* @Description: 查询多行数据
	* @param start
	* @param end
	* @return
	 */
	ResultScanner getData(String start, String end);
	
	/**
	 * 
	* @Title: getData
	* @Description: 查询多行数据
	* @param start
	* @param end
	* @param family
	* @param qualifier
	* @return
	 */
	ResultScanner getData(String start, String end, String family, String qualifier);
	
	/**
	 * 
	* @Title: getData
	* @Description: TODO
	* @param scan
	* @return
	 */
	ResultScanner getData(Scan scan);
	
}
