/**
* @Title: IBaseDao.java
* @Description: TODO
* @author: Calvinyang
* @date: Mar 28, 2015 8:37:00 PM
* Copyright: Copyright (c) 2013
* @version: 1.0
*/
package cn.edu.fudan.dsm.basic.dao.i;

import cn.edu.fudan.dsm.basic.bean.i.IBaseBean;
import cn.edu.fudan.dsm.basic.model.CriteriaList;

import java.util.List;

/**
 * @author: Calvinyang
 * @Description: TODO
 * @date: Mar 28, 2015 8:37:00 PM
 */
public interface IBaseDao<T extends IBaseBean> {
	/**
	 * 
	* @Title: setClass
	* @Description: TODO
	* @param cls
	 */
	Class<T> getClazz();
	
	/**
	 * 
	* @Title: add
	* @Description: TODO
	* @param obj
	 */
	int add(T obj);
	
	/**
	 * 
	* @Title: get
	* @Description: TODO
	* @return
	 */
	T get(int id);
	
	/**
	 * 
	* @Title: get
	* @Description: TODO
	* @param conditions
	* @return
	 */
	T get(CriteriaList conditions);
	
	/**
	 * 
	* @Title: getList
	* @Description: TODO
	* @return
	 */
	List<T> getList(CriteriaList conditions);
	
	/**
	 * 
	* @Title: getList
	* @Description: TODO
	* @param conditions
	* @param start
	* @param limit
	* @return
	 */
	List<T> getList(CriteriaList conditions, int start, int limit);
}
