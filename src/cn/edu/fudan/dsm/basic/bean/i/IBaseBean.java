/**
* @Title: IBaseBean.java
* @Description: TODO
* @author: Calvinyang
* @date: Mar 28, 2015 8:19:01 PM
* Copyright: Copyright (c) 2013
* @version: 1.0
*/
package cn.edu.fudan.dsm.basic.bean.i;

/**
 * @author: Calvinyang
 * @Description: TODO
 * @date: Mar 28, 2015 8:19:01 PM
 */
public interface IBaseBean {
	/**
	 * 
	* @Title: getId
	* @Description: TODO
	* @return
	 */
	int getId();
}
