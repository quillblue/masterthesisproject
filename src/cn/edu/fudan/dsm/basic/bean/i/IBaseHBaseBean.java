/**
* @Title: IBaseHBaseBean.java
* @Description: TODO
* @author: Calvinyang
* @date: Mar 28, 2015 10:38:27 PM
* Copyright: Copyright (c) 2013
* @version: 1.0
*/
package cn.edu.fudan.dsm.basic.bean.i;

import net.sf.json.JSONObject;
import org.apache.hadoop.hbase.client.Result;


/**
 * @author: Calvinyang
 * @Description: TODO
 * @date: Mar 28, 2015 10:38:27 PM
 */
public interface IBaseHBaseBean {
	/**
	 * 
	* @Title: getFamilies
	* @Description: TODO
	* @return
	 */
	String[] getFamilies();
	
	/**
	 * 
	* @Title: getColumns
	* @Description: TODO
	* @return
	 */
	String[] getColumnsForFamily(String family);
	
	/**
	 * 
	* @Title: translate
	* @Description: TODO
	* @param result
	* @return
	 */
	void translate(Result result);
	
	void postProcess(JSONObject config);
}
