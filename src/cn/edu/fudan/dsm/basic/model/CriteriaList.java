/**
* @Title: CriteriaList.java
* @Description: TODO
* @author: Calvinyang
* @date: Jan 6, 2015 11:10:13 AM
* Copyright: Copyright (c) 2013
* @version: 1.0
*/
package cn.edu.fudan.dsm.basic.model;

import java.util.ArrayList;

/**
 * @author: Calvinyang
 * @Description: TODO
 * @date: Jan 6, 2015 11:10:13 AM
 */
@SuppressWarnings("serial")
public class CriteriaList extends ArrayList<Object> {
	
	public static CriteriaList create(Object criteria) {
		CriteriaList list = new CriteriaList();
		list.add(criteria);
		return list;
	}
	
	public CriteriaList put(Object criteria) {
		add(criteria);
		return this;
	}
}
