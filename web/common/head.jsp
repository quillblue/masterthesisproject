<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- 
* @Title: head.jsp
* Modified by Quillblue on 2015-07-29
* This is a Partial Page.
-->

<!-- static navbar -->
<div class="navbar">
    <div class="navbar-inner">
        <div class="container-fluid"
             style="font-size: 18px; font-family: 微软雅黑, Arial;">
            <button type="button" class="btn btn-navbar" data-toggle="collapse"
                    data-target=".nav-collapse">
                <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                    class="icon-bar"></span>
            </button>
            <a class="brand" href="#">TimeSeriesDataVis</a>

            <div class="nav-collapse navbar-responsive-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a href="timeseries.jsp">SPTCC Data</a></li>
                </ul>
            </div>
            <div class="nav-collapse navbar-responsive-collapse collapse">
                <ul class="nav navbar-nav pull-right">
                    <!--<li><a href="aboutUs.jsp">AboutUs</a></li>-->
                </ul>
            </div>
        </div>
    </div>
</div>


