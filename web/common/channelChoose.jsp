%--
  User: Quillblue
  Date: 2015-03-29
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<html>
<head>
<title>ChannelTree</title>
</head>
<body>
	<ul class="breadcrumb margin-reset">
		<!-- style="height: 30px;" -->
		<ul class="nav nav-tabs margin-reset" id="tabs">
			<li class="active"><a id="treeTab1" href="#tab1" data-toggle="tab">All</a></li>
			<li><a id="treeTab2" href="#tab2" data-toggle="tab">Search</a></li>
			<li><a id="treeTab3" href="#tab3" data-toggle="tab">Chosen</a></li>
		</ul>
	</ul>
	<div class="tab-content">
		<div id="tab1" class="tab-pane fade in active">
			<ul id="tree" class="ztree">
			</ul>
		</div>
		<div id="tab2" class="tab-pane fade in">
			<div class="channel_search">
				<input class="span9" id="search-keyword" type="text" placeholder="搜索关键字" value="8AT003-DZ"> <br/><input id="channelSearchBtn" type="button" class="btn btn-primary" value="Search" />
			</div>
			<div id="searchTip"></div>
			<ul id="treeSearchResult">
			</ul>
		</div>
		<div id="tab3" class="tab-pane fade in">
			<div id="tip">暂未选择任何通道</div>
			<ul id="channelChosen"></ul>
		</div>
	</div>
</body>
</html>
