<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Time Series Data</title>

    <link href="../assets/images/icon.ico" type="image/x-icon" rel=icon/>
    <jsp:include page="../common/top.html"></jsp:include>

    <link rel="stylesheet" href="../assets/css/masterpage.css" type="text/css"/>
</head>

<body>
<!--公共页面-->
<jsp:include page="../common/head.jsp"></jsp:include>

<div class="container-fluid">
    <div class="row-fluid">
        <div style="margin-left:-1px; margin-right:-1px; margin-top: 3px;">
            Select query range
        <jsp:include page="timePicker.jsp"></jsp:include>
            <div id="datetimepickersubmit" class="input-append date">
                <a class="btn btn-primary btn-block" href="javascript:void(0);" id="submit" style="width:100px;">Submit</a>

            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="span8" role="container" style="height:440px;">
                <div id="mainChart" style="margin-left: 0; margin-right: 0; height: 440px;border:1px #ccc solid;">
                </div>
            </div>
            <div class="span4" role="container">
                <div class="interestChart"
                     style=" height: 220px;border:1px #ccc solid;">
                    <div class="rangeDigramHead"><span>Range A</span><a class="btn btn-primary" style="float: right;" href="javascript:void(0);">View Detail</a></div>
                </div>
                <div class="interestChart"
                     style="height: 220px;border:1px #ccc solid;">
                    <div class="rangeDigramHead"><span>Range B</span><a class="btn btn-primary" style="float: right;" href="javascript:void(0);">View Detail</a></div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- container -->


<jsp:include page="../common/foot.jsp"></jsp:include>
<script type="text/javascript">
    var active = 2;
</script>
<!--<script type="text/javascript" src="../assets/js/chooseChannelId.js"></script>-->
<script src="../assets/js/global.js"></script>
<script src="../assets/plugin/highcharts/js/highcharts.js"></script>
<script src="../assets/plugin/highcharts/js/modules/exporting.js"></script>
<script type="text/javascript" src="../assets/js/drawLine.js"></script>
<script type="text/javascript" src="../assets/js/historyMonitor.js"></script>
</body>
</html>