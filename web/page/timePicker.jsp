<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
    <div id="datetimepicker1" class="input-append date">
        <input placeholder="To Select StartTime:" data-format="yyyy-MM-dd hh:mm:ss"
               type="text" class="input-medium" id="startTime" value="2015-04-01 00:00:00"
               style="width:78%;margin-right:0px;"/>
					<span class="add-on" style="width:10%;"> 
						<i style="margin-top: 5px;" data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
					</span>
    </div>
    <div id="datetimepicker2" class="input-append date">
        <input placeholder="To Select EndTime:" data-format="yyyy-MM-dd hh:mm:ss"
               type="text" class="input-medium" id="endTime" value="2015-05-01 00:00:00"
               style="width:78%;margin-right:0px;"/>
					<span class="add-on" style="width:10%;"> 
						<i style="margin-top: 5px;" data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
					</span>
    </div>
    <!-- datetimepicker2 -->

<!-- container -->

<script type="text/javascript">
    $('#datetimepicker1,#datetimepicker2').datetimepicker({
        format: 'yyyy-MM-dd hh:mm:ss',
        language: 'en',
        pickDate: true,
        pickTime: true,
        hourStep: 1,
        minuteStep: 15,
        secondStep: 30,
        inputMask: true
    });
</script>