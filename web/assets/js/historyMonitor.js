var History = {
	init : function() {
		// submit_his
		$(function() {
			$("#clearLog").click(function() {
				$("#logList li").remove();
			});
			$("#submit").click(function() {
				var cmds = [];
				cmds.push({
					startTime : new Date($("#datetimepicker1 input").val()).getTime(),
					endTime : new Date($("#datetimepicker2 input").val()).getTime()
				});
				Chart.drawSeries(cmds, true);
			});
		});
	}
};

$(function() {
	History.init();
});