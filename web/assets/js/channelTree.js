﻿/**
 * Created by Quillblue on 4/3/2015.
 */

var searchUrl = 'channelSearch_search.action?isAjax=1&rand=' + Math.random();
var getSeqUrl='channelSearch_getByCode.action?isAjax=1&rand='+Math.random();

var ChannelTree = {
    treeObj : null,
    treeObjSelected : null,
    treeSelectedRoot : null,
    nodeSelected : null,
    url : 'channel_list.action?isAjax=1&rand=' + Math.random(),
    selectedUrl : 'channel_get.action?isAjax=1&rand=' + Math.random(),
    selectedAdd : 'channel_add.action?isAjax=1&rand=' + Math.random(),
    selectedRemove : 'channel_remove.action?isAjax=1&rand=' + Math.random(),

    init : function() {
        // 所有通道树
        ChannelTree.treeObj = $.fn.zTree.init($("#tree"), allChannelTree.setting,
            allChannelTree.nodes);
        var rootNode = ChannelTree.treeObj.getNodeByParam("level", 0);
        ChannelTree.treeObj.expandNode(rootNode, true, true, true,true);
    },
    nodeChange : function(treeNode) {
        ChannelTree.nodeSelected = treeNode;
        $("#nodeSelected").trigger('change');
    },
    nodeDelete : function(code) {
        $("#nodeDeleted").val(code).trigger('change');
    }
};

var allChannelTree={
    setting : {
        async : {
            enable : true,
            dataType : 'json',
            url : ChannelTree.url,
            autoParam : [ 'code' ],
            dataFilter : function(treeId, parentNode, ret) {
                if (ret.r != 1) {
                    alert("通道拉取失败!");
                    return;
                }
                var data = ret.data.channelList;
                for (var i = 0; i < data.length; i++) {
                    data[i].title = data[i].description;
                    if (data[i].title == null) {
                        data[i].title = data[i].name;
                    }
                }
                return data;
            }
        },
        view : {
            selectMulti : false,
            fontCss : function(treeId, treeNode) {
                return (!!treeNode.highlight) ? {
                    color : "#A60000",
                    "font-weight" : "bold"
                } : {
                    color : "#333",
                    "font-weight" : "normal"
                };
            }
        },
        edit : {
            enable : true
        },
        data : {
            simpleData : true,
            key : {
                title : "title"
            }
        },
        callback : {
            beforeDrag : function(treeId, treeNodes) {
                return false;
            },
            beforeExpand : function(treeId, treeNode) {
                var nodes = ChannelTree.treeObj.getNodesByParam("level",
                    treeNode.level);
                for (var i = 0; i < nodes.length; i++) {
                    if (nodes[i].open = true && nodes[i] != treeNode) {
                        ChannelTree.treeObj.expandNode(nodes[i], false);
                    }
                }
            },
            onExpand : function(event, treeId, treeNode) {
                //$("#search-keyword").trigger("keyup");
            },
            onClick:function(event, treeId, treeNode) {
                if (treeNode.isParent) {
                    var zTree = $.fn.zTree.getZTreeObj("tree");
                    zTree.expandNode(treeNode, null, null, null, true);
                    ChannelTree.selectedNode = treeNode;
                }
            },
            onDblClick : function(event, treeId, treeNode) {
                if (treeNode.isParent) {
                    return;
                }
                ChannelTree.nodeChange(treeNode);

                selectedChannelList.addByInfo(treeNode.code,treeNode.name,treeNode.nameseq);
                GLOBAL.ajaxReq({
                    url : ChannelTree.selectedAdd,
                    data : {
                        code : treeNode.code
                    },
                    loading : 1,
                    callback : function() {}
                });
            }
        }
    },
    nodes : [ {
        name : "所有通道",
        title : "所有通道",
        isParent : true,
        code : "63a9f0ea7bb98050796b649e85481845",
        id:-1,
        pId : -1
    } ]
};

var selectedChannelList={
    data:[],
    contains:function(code){
        for(i=0;i<selectedChannelList.data.length;i++){
            if(selectedChannelList.data[i]==code){return true;}
        }
        return false;
    },
    addByInfo:function(code,name,nameseq){
        if(selectedChannelList.contains(code)){alert("已经添加过该通道！");return;}
        if(this.data.length==0){$("#tip").hide();}
        this.data.push(code);
        var addObj='<li id="chosen_'+code+'" class="treeListItem">'+name+'<span class="channelOp"><a id="'+code+'_remove" href="javascript:void(0)"">删除</a></span><p class="levelSeqNote">'+nameseq+'</p></li>';

        $("#channelChosen").append(addObj);
        var btn=$("#"+code+"_remove");
        if(btn){
            btn.bind("click",function(){
                $("#chosen_"+code).remove();
                for(i=0;i<selectedChannelList.data.length;i++){
                    if(selectedChannelList.data[i]==code){selectedChannelList.data.splice(i,1);}
                }
                if(selectedChannelList.data.length==0){$("#tip").show();}
            });
        }
    }
};


$(document).ready(function(){
    ChannelTree.init();
    $("#channelSearchBtn").bind("click",function(){
        GLOBAL.ajaxReq({
            url : searchUrl,
            data : {
                keyword : $("#search-keyword").val()
            },
            loading : 1,
            callback : function(data) {
                $("#treeSearchResult").empty();
                $("#searchTip").html("查询得到结果"+data.resultCount+"条");
                if(data.resultCount>50){$("#searchTip").append("，此处仅显示前50条")};
                for(i=0;i<data.matchedList.length;i++){
                    var addObj='<li class="treeListItem">'+data.matchedList[i].name+' <span class="channelOp"><a id="'+data.matchedList[i].code+'_add" href="javascript:void(0)">添加该通道</a> | <a id="'+data.matchedList[i].code+'_treedisplay" href="#">在树上显示</a></span><p class="levelSeqNote">'+data.matchedList[i].nameseq+'</p></li>';
                    $("#treeSearchResult").append(addObj);
                    var addBtn=$("#"+data.matchedList[i].code+"_add");
                    if(addBtn) {
                        addBtn.bind("click", function () {
                            selectedChannelList.addByInfo(this.id.split('_')[0], this.parentElement.parentElement.innerText.split(' ')[0], this.parentElement.parentElement.children[1].innerText);
                            //alert('添加成功');
                        });
                    }
                    var treeDisplayBtn=$("#"+data.matchedList[i].code+"_treedisplay");
                    if(treeDisplayBtn) {
                        treeDisplayBtn.bind("click", function () {
                            GLOBAL.ajaxReq({
                                url:getSeqUrl,
                                data:{
                                    code:this.id.split('_')[0]
                                },
                                loading:1,
                                callback:function(data){
                                    $("#treeTab1").trigger("click");
                                    var levelSeq=data.data.levelseq;
                                    var levelSeqArray=levelSeq.split(' ');
                                    var zTree = $.fn.zTree.getZTreeObj("tree");
                                    var j=0;
                                    for(i=0;i<levelSeqArray.length;i++){
                                        setTimeout(function(){$("#"+ChannelTree.treeObj.getNodeByParam("code",levelSeqArray[j]).tId+"_span").trigger("click");j++;},500*i);
                                    }
                                }
                            })
                            //alert(2);
                        })
                    }
                }
            }
        });
    });

});
