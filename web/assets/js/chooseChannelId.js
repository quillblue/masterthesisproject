var ChannelTree = {
	treeObj : null,
	treeObjChosen : null,
	selectedNode : null,
	url : '../tree_list.action',
	init : function() {
		ChannelTree.treeObj = $.fn.zTree.init($("#tree"), setting, zNodes);
		var rootNode = ChannelTree.treeObj.getNodeByParam("level", 0);
		ChannelTree.treeObj.expandNode(rootNode, true, false, true);

		ChannelTree.treeObjChosen = $.fn.zTree.init($("#treeChosen"), setting_chosen, zNodesChosen);
		var rootNode = ChannelTree.treeObjChosen.getNodeByParam("level", 0);
		ChannelTree.treeObjChosen.expandNode(rootNode, true, false, true);
		//展开或者折叠指定的节点，Function(treeNode, expandFlag, sonSign, focus, callbackFlag)
		//expandFlag = true 表示 展开 节点，
		//sonSign = false 表示 只影响此节点，对于其 子孙节点无任何影响
		//focus = true 表示 展开 / 折叠 操作后，通过设置焦点保证此焦点进入可视区域内
		//callbackFlag = false 表示执行此方法时不触发事件回调函数，默认false
	},
	nodeChange : function(treeNode) {
		ChannelTree.nodeSelected = treeNode;
		$("#nodeSelected").trigger('change');
	},
	nodeDelete : function(code) {
		$("#nodeDeleted").val(code).trigger('change');
	}
};
var setting = {
	async : {
		enable : true,
		dataType : 'json',
		url : ChannelTree.url,
		autoParam : [ 'code' ],
		dataFilter : function(treeId, parentNode, ret) {
			for ( var i = 0; i < ret.length; i++) {
				ret[i].title = ret[i].name + ret[i].code;
			}
			return ret;
		}
	},
	view : {
		dblClickExpand : false, //关闭双击展开节点的功能
		showLine : true, //由于 +/-开关与 节点连接线是配套的，所以如果不显示 +/-开关的话，
		//那么设置 setting.view.showLine = false 隐藏连接线
		showIcon : true
	//默认为true，自定义图标不需要对 setting 进行特殊配置
	},
	data : { //用来控制使用的数据是否是简单数据格式——数组
		simpleData : {
			enable : false
		}
	},
	callback : { // 此两行代码用来控制保持单一路径
		beforeExpand : beforeExpand,
		onExpand : onExpand,
		onClick : onClick,
		onDblClick : function(event, treeId, treeNode) {
			if (treeNode.isParent) {
				return;
			}
			ChannelTree.nodeChange(treeNode);
			if (ChannelTree.treeObjChosen.getNodeByParam("code", treeNode.id) != null) {
				return;
			}
			var parentNode = treeNode.getParentNode();
			ChannelTree.treeObjChosen.addNodes(ChannelTree.treeSelectedRoot,
					{
						name : "(" + treeNode.code + ")" + treeNode.name,
						title : treeNode.code,
						id : treeNode.id
					}, false);
			// 发请求通知选了新通道
//			GLOBAL.ajaxReq({
//				url : ChannelTree.selectedAdd,
//				data : {
//					channelId : treeNode.id
//				},
//				loading : 1,
//				callback : function() {
//
//				}
//			});
		}
	},
	check : {
		enable : true,
	}
};

var zNodes = [ {
	name : "所有通道",
	title : "所有通道",
	isParent : true,
	code : '63a9f0ea7bb98050796b649e85481845',
} ];
var setting_chosen = {
	async : {
		enable : false,
		dataType : 'json',
		url : ChannelTree.url,
		autoParam : [ 'code' ],
		dataFilter : function(treeId, parentNode, ret) {
			for ( var i = 0; i < ret.length; i++) {
				ret[i].title = ret[i].name + ret[i].code;
			}
			return ret;
		}
	},
	view : {
		dblClickExpand : false, //关闭双击展开节点的功能
		showLine : true, //由于 +/-开关与 节点连接线是配套的，所以如果不显示 +/-开关的话，
		//那么设置 setting.view.showLine = false 隐藏连接线
		showIcon : true
	//默认为true，自定义图标不需要对 setting 进行特殊配置
	},
	data : { //用来控制使用的数据是否是简单数据格式——数组
		simpleData : {
			enable : false
		}
	},
	callback : { // 此两行代码用来控制保持单一路径
		beforeExpand : beforeExpand,
		onExpand : onExpand,
	},
	check : {
		enable : true,
	}
};
var zNodesChosen = [ {
	name : "已选通道",
	title : "已选通道",
	isParent : true,
	open : true,
	id : -1,
	pId: -1
} ];
var curExpandNode = null;
function beforeExpand(treeId, treeNode) {
	var pNode = curExpandNode ? curExpandNode.getParentNode() : null;
	var treeNodeP = treeNode.parentTId ? treeNode.getParentNode() : null;
	var zTree = $.fn.zTree.getZTreeObj("tree");
	for ( var i = 0, l = !treeNodeP ? 0 : treeNodeP.children.length; i < l; i++) {
		if (treeNode !== treeNodeP.children[i]) {
			zTree.expandNode(treeNodeP.children[i], false);
		}
	}
	while (pNode) {
		if (pNode === treeNode) {
			break;
		}
		pNode = pNode.getParentNode();
	}
	if (!pNode) {
		singlePath(treeNode);
	}
}
function singlePath(newNode) {
	if (newNode === curExpandNode)
		return;
	if (curExpandNode && curExpandNode.open == true) {
		var zTree = $.fn.zTree.getZTreeObj("tree");
		if (newNode.parentTId === curExpandNode.parentTId) {
			zTree.expandNode(curExpandNode, false);
		} else {
			var newParents = [];
			while (newNode) {
				newNode = newNode.getParentNode();
				if (newNode === curExpandNode) {
					newParents = null;
					break;
				} else if (newNode) {
					newParents.push(newNode);
				}
			}
			if (newParents != null) {
				var oldNode = curExpandNode;
				var oldParents = [];
				while (oldNode) {
					oldNode = oldNode.getParentNode();
					if (oldNode) {
						oldParents.push(oldNode);
					}
				}
				if (newParents.length > 0) {
					zTree.expandNode(oldParents[Math.abs(oldParents.length
							- newParents.length) - 1], false);
				} else {
					zTree.expandNode(oldParents[oldParents.length - 1], false);
				}
			}
		}
	}
	curExpandNode = newNode;
}

function onExpand(event, treeId, treeNode) {
	curExpandNode = treeNode;
}

function onClick(e, treeId, treeNode) {
	var zTree = $.fn.zTree.getZTreeObj("tree");
	zTree.expandNode(treeNode, null, null, null, true);
	ChannelTree.selectedNode = treeNode;
}

$(document).ready(function() { //用来初始化树
	//$.fn.zTree.init($("#treeChoose"), setting_choose, zNodesChoose);
	ChannelTree.init();
});