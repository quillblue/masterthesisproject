Date.prototype.format = function(format) {
	var o = {
		"M+" : this.getMonth() + 1, // month
		"d+" : this.getDate(), // day
		"h+" : this.getHours(), // hour
		"m+" : this.getMinutes(), // minute
		"s+" : this.getSeconds(), // second
		"q+" : Math.floor((this.getMonth() + 3) / 3), // quarter
		"S" : this.getMilliseconds()
	// millisecond
	};
	if (/(y+)/.test(format))
		format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
	for ( var k in o)
		if (new RegExp("(" + k + ")").test(format))
			format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
	return format;
};

var Chart = {
	cachedCmd : [],
	chartObj : null,
	nonZoomable : false,
	init : function() {
		Highcharts.setOptions({
			global : {
				useUTC : false
			},
			lang : {
				resetZoom : "上一步"
			}
		});
		Chart.cachedCmd = [];
		Chart.chartObj = null;
	},
	drawSeries : function(cmds, hideZoom) {
		Chart.redraw();
		Chart.cachedCmd.push(cmds);
		for (var i = 0; i < cmds.length; i++) {
			$.ajax({
				url : "/display_display?rand=" + Math.random(),

				loading : 1,
				data : cmds[i],
				type : "post",
				dataType : 'json',
				success : function(ret) {
					// ret实际为JSON，其中包含channel和data
					var dataList = [];
					dataList=ret.data.list;
					Chart.chartObj.addSeries({
						name : 'Series 1',
						data : dataList
					});
				}
			});
		}
		// 是否显示缩放按钮
		if (!hideZoom) {
			Chart.chartObj.showResetZoom();
		}
	},

	showChart : function() {
		// historyChart = new Highcharts.StockChart({
		$("#mainChart").highcharts({
			chart : {
				events : {
					load : function() {
						Chart.chartObj = this;
					},
					selection : function(event) {
						if (event.resetSelection) {
							// preStep
							var cmds = Chart.cachedCmd.pop(); // del current
							// cmds
							if (Chart.cachedCmd.length > 0) {
								cmds = Chart.cachedCmd.pop();
							}
							Chart.drawSeries(cmds, Chart.cachedCmd.length == 0);
						} else {
							// 放大
							var x = parseInt(event.xAxis[0].min);
							var y = parseInt(event.xAxis[0].max);
							if (y - x < 10 * 1000) {
								alert("No more data with finer granularity!");
								return false;
							}
							// var preCmds = $("#Chosen li");
							var cmds = [];
							var preCmds;
							if (Chart.cachedCmd.length > 0)
								preCmds = Chart.cachedCmd[Chart.cachedCmd.length - 1];
							if (preCmds[0].hasOwnProperty('windowSize')) {
								for (var i = 0; i < preCmds.length; i++) {
									cmds.push({
										startTime : x,
										endTime : y,
										windowSize : preCmds[i].windowSize,
										windowType : preCmds[i].windowType,
										inputTarget : preCmds[i].inputTarget
									});
								}
							} else {
								for (var i = 0; i < preCmds.length; i++) {
									cmds.push({
										startTime : x,
										endTime : y
									});
								}
							}
							Chart.drawSeries(cmds);
						}
					}
				},
				zoomType : Chart.nonZoomable ? null : 'x'
			},
			rangeSelector : {
				buttons : [{
					count : 1,
					type : 'hour',
					text : '1 hour'
				}, {
					count : 1,
					type : 'day',
					text : '1 day'
				},{
					count : 1,
					type : 'month',
					text : '1 mon'
				},{
					count : 'all',
					text : 'all'
				} ],
				inputEnabled : false,
				selected : 5
			},
			plotOptions : {
				series : {
					turboThreshold : 1000000
				}
			},
			legend : {
				enabled : false
			},
			xAxis : {
				title : {
					align : 'high',
					text : 'Time'
				},
				label : {
					rotation : -45
				},
				type: 'datetime'
			},
			yAxis : {
				title : {
					text : 'Value'
				}
			},
			tooltip : {
				enabled : true,
				formatter : function() {
					return "时间:<b>" + new Date(this.x).format('yyyy-MM-dd h:m:s.S') + "</b><br/>数值:<b>" + new Number(this.y).toFixed(3) + "</b>";
				}
			},
			series : []
		});
	},
	redraw : function(loading, zoom) {
		if (Chart.chartObj != null) {
			for (var i = 0; i < Chart.chartObj.series.length; i++) {
				Chart.chartObj.series[i].remove(false);
			}
			Chart.chartObj.destroy();
			Chart.chartObj = null;
		}
		Chart.zoom = zoom != null;
		Chart.showChart();
	}
};

$(function() {
	Chart.init();
});