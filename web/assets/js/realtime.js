var chartObj = null;

Date.prototype.format = function(format) {
	var o = {
		"M+" : this.getMonth() + 1, // month
		"d+" : this.getDate(), // day
		"h+" : this.getHours(), // hour
		"m+" : this.getMinutes(), // minute
		"s+" : this.getSeconds(), // second
		"q+" : Math.floor((this.getMonth() + 3) / 3), // quarter
		"S" : this.getMilliseconds()
	// millisecond
	};
	if (/(y+)/.test(format))
		format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
	for ( var k in o)
		if (new RegExp("(" + k + ")").test(format))
			format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
	return format;
};

function drawChart() {
	Highcharts.setOptions({
		global : {
			useUTC : false
		}
	});
	$("#historyChart").highcharts('StockChart', {
		chart : {
			events : {
				load : function() {
					chartObj = this;
					
					var seriesArray =  this.series;
					// 定时获取数据
					setInterval(function() {
						for(var i = 0 ; i < seriesArray.length; i ++) {
							(function() {
								var thisSeries = seriesArray[i];
								if(thisSeries.name.indexOf("Nav") >= 0) {
									return ;
								}
								console.log(thisSeries.name + " => " + new Date().format("yyyy-MM-dd h:mm:ss.S"));
								$.ajax({
									url : "channelRealtime_display.action?rand=" + Math.random(),
									data : {
										code : thisSeries.name
									},
									type : "GET",
									dataType : "json",
									success : function(ret) {
										if(ret.data.list.length == 0) {
											return;
										}
										var startT = new Date().getTime();
										for (var ii = 0; ii < ret.data.list.length; ii++) {
											thisSeries.addPoint([ ret.data.list[ii].timestamp, ret.data.list[ii].value ], true, false);
										}
										console.log("点的数量 : " + ret.data.list.length + " 画图耗时 : " + (new Date().getTime() - startT));
										$("li.lastLog").removeClass("lastLog");
										$("#logList").append("<li class='lastLog'>" + thisSeries.name + " => " + ret.data.list.length + " 条数据" + " 画图耗时 : " + (new Date().getTime() - startT) / 1000.0 + "秒</li>");
										$("#logList").scrollTop(100000000);
									}
								});
							})();
						}
					}, 500);
				}
			}
		},

		rangeSelector : {
			buttons : [],
			inputEnabled : false,
			selected : 0
		},

		title : {
			text : '通道实时数据'
		},

		legend : {
			enabled : true,
			layout : 'horizontal',
			align : 'center'
		},
		
		exporting : {
			enabled : false
		},
		
		tooltip : {
			enabled : true,
			formatter : function() {
				return "时间:<b>" + new Date(this.x).format('yyyy-MM-dd h:m:s.S') + "</b><br/>数值:<b>" + new Number(this.y).toFixed(3) + "</b>";
			}
		},
		
		series : []
	});
}

// 监控添加通道的事件
function monitor() {
	if (selectedChannelList == null || selectedChannelList.data == null || selectedChannelList.data.length == 0) {
		return;
	}
	var channels = selectedChannelList.data;
	// 删除未选的通道
	for (var i = 0; i < chartObj.series.length; i++) {
		var c = chartObj.series[i].name;
		if(c.indexOf("Nav") >= 0) {
			continue;
		}
		var hasFind = false;
		for (var j = 0; j < channels.length; j++) {
			if (channels[j] == c) {
				hasFind = true;
				break;
			}
		}
		if (!hasFind) {
			chartObj.series[i].remove(false);
		}
	}
	// 添加已选的通道
	for(var i = 0 ; i < channels.length ; i ++) {
		var hasFind = false;
		for(var j = 0 ; j < chartObj.series.length ; j ++) {
			var c = chartObj.series[j].name;
			if(c == channels[i]) {
				hasFind = true;
				break;
			}
		}
		if(!hasFind) {
			chartObj.addSeries({name : channels[i], data: []});
		}
	}
}

// 1秒每次获取历史数据
$(function() {
	$("#search-keyword").val("6CT112-D");
	drawChart();
	setInterval(monitor, 500);
//	setInterval(function() {
//		var t = new Date().getTime();
//		var data = [];
//		for(var i = 0 ; i < 50 ; i ++) {
//			data.push([t + i * 19, Math.random()]);
//		}
//		var series = null;
//		for(var i = 0 ; i < chartObj.series.length ; i++) {
//			if(chartObj.series[i].name == "8AT003-DZ") {
//				series = chartObj.series[i];
//			}
//		}
//		if(series == null) {
//			series = chartObj.addSeries({name : "8AT003-DZ" , data : []});
//		}
//		for(var i = 0 ; i < data.length ; i ++) {
//			series.addPoint(data[i]);
//		}
//	}, 1000);
});