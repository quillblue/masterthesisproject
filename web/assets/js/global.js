/**
 * Created by Quillblue on 4/3/2015.
 */

var GLOBAL={
    // 异步请求函数
    ajaxReq : function(params) {
        // 设置页码
        params.data = GLOBAL.getObject(params.data);
        // 发请求
        if (params.loading == undefined) {}
        $.ajax({
            url : params.url + (params.url.indexOf("?") >= 0 ? '&' : '?')
            + "isAjax=1&rand=" + Math.random(),
            type : 'post',
            dataType : 'json',
            data : params.data,
            success : function(ret) {
                // 请求成功
                //$.jBox.closeTip();
                switch (parseInt(ret.r)) {
                    case 1:
                        if (params.callback != undefined) {
                            params.callback(ret.data);
                        } else {
                            alert("操作成功!");
                        }
                        // 记录log
                        if(ret.log != null) {
                            GLOBAL.log(ret.log);
                        }
                        break;
                    default:
                        GLOBAL.error(ret.msg);
                        break;
                }
            },
            error : function() {
                alert("系统忙碌，请稍候再试!");
            }
        });
    },
    getObject : function(data) {
        return data == undefined ? {} : data;
    },
    tip : function(data) {
        //$.jBox.tip(data, {
        //    title : "提示"
        //}, {
        //    timeout : 2000
        //});
    }
}

$(document).ready(function(){
    //GLOBAL.init();
})