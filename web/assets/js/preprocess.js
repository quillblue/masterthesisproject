var chart;
$(function() {
	$("#preprocess")
			.click(
					function() {
						var treeObj = $.fn.zTree.getZTreeObj("treeChosen");
						var nodesChosen = treeObj.getNodesByParam("level", 0);
						if (nodesChosen == null || nodesChosen.length < 2) {
							alert("Please select the Channel!");
							return;
						}
						if ($("#datetimepicker1 input").val() == "") {
							alert("Please select the start time!");
							return;
						}
						if ($("#datetimepicker2 input").val() == "") {
							alert("Please select the end time!");
							return;
						}
						if (new Date($("#datetimepicker1 input").val())
								.getTime() > new Date($(
								"#datetimepicker2 input").val()).getTime()) {
							alert("The start time is after the end time, please select again!");
							return;
						}
						var cmds = [];
						for ( var i = 1; i < nodesChosen.length; i++) {
							cmds.push({
								channelId : nodesChosen[i].title,
								starttime : $("#datetimepicker1 input").val(),
								endtime : $("#datetimepicker2 input").val()
							});
						}
//						Force to redraw
						chart = null;
						drawSeries(cmds);
					});		//end click
});		//end function
	

var count = 1; 	//To control the position of every line.
var xset = []; 	//To control the view of xaxis.
var yset = [];	//To control the view of yaxis.
var retArray = [];	// To store the data has been returned.
var drawSeries = function(cmds) {
	retArray = [];
	yset = [""];
	count = 1;
	//		To get xset
	$.ajax({
		url : "DisplayPreProcess",
		async:false,
		type : "get",
		dataType : "json",
		data : cmds[0],
		success : function(ret) {
			xset = [];
			for ( var ii = 0; ii < ret.data.length; ii++) {
				xset[xset.length] = ret.data[ii].time;
//						time : ret.data[ii].time,
//						index : ii
//				};
			}
			;
		}
	});
	//		create a new chart
	if (chart == null) {
		drawChart(retArray,xset,yset);
	}
	//		To get the data and add series.
	for ( var i = 0; i < cmds.length; i++) {
		$.ajax({
			url : "DisplayPreProcess",
			type : "get",
			dataType : "json",
			data : cmds[i],
			success : function(ret) {
				var dataList = [];
				for ( var ii = 0; ii < ret.data.length; ii++) {
					dataList[dataList.length] = {
						name : ret.data[ii].time,
						y : count,
						color :	'rgb('
							+ 255 + ','
							+ Math.abs(parseInt(255-ret.data[ii].value*255)) + ','
							+ 0 + ')',
						value : ret.data[ii].value
//						The color ranges from yellow to red, changed by ret.data[ii].value from 0 to 1. 
					};
				}
				count++;
				//					add Series
				var isExisted = false;
				for ( var i = 0; i < chart.series.length; i++) {
					if (chart.series[i].name == ret.channel) {
						isExisted = true;
						break;
					}
				}
				if (isExisted == false) {
					retArray[retArray.length] = {
							color : 'grey',
							data : dataList,
							name : ret.channel
							};
					yset[retArray.length] = ret.channel;
					yset[retArray.length+1] = "";
				};
				chart = null;
				drawChart(retArray,xset,yset);
			}
		}); // end ajax
	} // end for
}; // end drawSeries
function drawChart(retArray,xset,yset) {
	$("#proPrecessChart").highcharts({
		chart : {
			events : {
				load : function() {
					chart = this;
				}
			}
		},
		title : {
			text : 'The Loss Percent of the Original Data',
		},
		plotOptions : {
			series : {
				turboThreshold : 1000000
			},
		},
		xAxis : {
			categories : xset,
			labels : {
				step : xset.length - 1
			},
			title : {
				text : 'Time'
			},
		},
		yAxis : {
			categories : yset,
			min : 0,
			startOnTick: false,
			max : yset.length-1,
			gridLineColor : '#FFFFFF',
			title : {
				text : 'Loss Percent'
			},
			plotLines : [ {
				value : 0,
				width : 1,
			} ],
		},
		tooltip : {
			enabled : true,
			formatter: function() {
                return this.x + '<br/>' + this.series.name;// + ':' + this.series.data[xx(this.x)].value;
            }
		},
		legend : {
			enabled : false,
			layout : 'vertical',
			align : 'right',
			verticalAlign : 'middle',
			borderWidth : 0
		},
		series : retArray,
	});
}

function xx(str){
	xx = [];
	for(var i=0; i<xset.length; i++){
		xx[i] = xset[i].time;
	}
	return xx;
}